package com.tiket.tix.events.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiket.tix.events.kafka.aspect.Producer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by Poornima on 3/27/18.
 */
@Aspect
@Component
public class KafkaAspect {
  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaAspect.class);

  @Autowired
  KafkaProducer kafkaProducer;

  @Autowired
  ObjectMapper objectMapper;

  @Around("@annotation(com.tiket.tix.events.kafka.aspect.Producer)")
  public void produceKafka(ProceedingJoinPoint joinPoint) throws Throwable {
    LOGGER.info("Inside kafka Producer AOP");
    try {
      Object returnValue = joinPoint.proceed();
      final String methodName = joinPoint.getSignature().getName();
      MethodSignature signature = (MethodSignature) joinPoint.getSignature();
      Method method = signature.getMethod();
      if (method.getDeclaringClass().isInterface()) {
        method = joinPoint.getTarget().getClass().getDeclaredMethod(methodName, method.getParameterTypes());
      }
      for(Annotation annotation : method.getDeclaredAnnotations()) {
        if (annotation instanceof Producer) {
          Producer producer = (Producer) annotation;
          String[] topics = producer.topics();

          for (String topic : topics) {
            kafkaProducer.send(topic, objectMapper.writeValueAsString(returnValue));
          }
        }
      }
    } catch (Exception ne) {
      LOGGER.error("Error at producer aspect", ne);
      throw ne;
    }
  }
  }





