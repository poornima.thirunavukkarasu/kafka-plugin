package com.tiket.tix.events.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * Created by Poornima on 3/28/18.
 */
@Configuration
@EnableKafka
@EnableAspectJAutoProxy
public class KafkaConfiguration {

  @Bean
  public KafkaAspect eventAspect() {
    return new KafkaAspect();
  }

  @Bean
  public KafkaProducer kafkaProducer(){
    return new KafkaProducer();
  }
}
